const defaultTheme = require('tailwindcss/defaultTheme')
/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./pages/**/*.{js,ts,jsx,tsx}",
        "./comps/**/*.{js,ts,jsx,tsx}",
      ],
  theme: {
    fontSize: {
      xs: '.55rem',
      sm: '.700rem',
      md: '.875rem',
      base: '1rem',
      lg: '1.125rem',
      xl: '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '4rem',
      '7xl': '5rem',
    },
    extend: {
      backgroundColor : ['checked'],
    },
    screens:{
      sm: '250px',
      sm1: '340px',
      md: '550px',
      md1: '1100px',
      lg: '1225px',
      xl: '1380px',
   '2xl': '1536px',
    }
  },
  plugins: [],
}
