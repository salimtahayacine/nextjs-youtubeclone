import Head from 'next/head'
import Image from 'next/image'
import Navbar from '../comps/Navbar'
import Sidebare from "../comps/Sidebare"
import styles from '../styles/Home.module.css'
import Contenus from '../comps/Contenus'
import SecondBare from '../comps/SecondBare'

export default function Home() {
  return (
    <div className='App'>
      <Head>
        <title>Youtube Clone</title>
      </Head>
      <Navbar/>
      <SecondBare/>
      <div className='flex'>
        <Sidebare />
        {<Contenus />}
      </div>
      
    </div>
  )
}
