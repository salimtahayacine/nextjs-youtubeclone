import { useEffect, useRef, useState } from 'react'
//import Video from './Video'
//import {Link} from 'react-dom';
import Link from 'next/link'
//import useFetch from './useFetch';
//import Navbar from './Navbar';
import ReactPlayer from "react-player";
import {VideoDatas,ShortDatas} from './data';
const Contenus = () => {
   //const {data:VideoInfos ,isPending , error} = useFetch('http://localhost:8000/VideoInfos');
   //Videos Data 
   const [showinfos,setShowInfos] = useState(true);
   //const [divShowd,setDivShowd] = useState(-1);

   useEffect(() => {
  
   },[])
   const VideoInfs = useRef(null);
   const HandlMouseEnter = (e,index) => {
    // console.log("target",e.target);
    //setShowInfos(true);
    //useRefs 
    VideoInfs.current.style.visibility = 'visible';
    
    //   const divshow1 = document.getElementById('VideoData'+ index);
    //   divshow1.style.visibility = 'visible';
    //   const divshow2 = document.getElementById('VideoData1'+ index);
    //   divshow2.style.visibility = 'visible'; 
  }
  const HandlMouseLeave = (e,index) => {
     //VideoInfs.style.visibility = 'hidden'

     VideoInfs.current.style.visibility = 'hidden';
    //   const divshow1 = document.getElementById('VideoData'+ index);
    //   divshow1.style.visibility = 'hidden';
    //   const divshow2 = document.getElementById('VideoData1'+ index);
    //   divshow2.style.visibility = 'hidden';

  }

  return (
    <div className='VideoList bg-stone-50 font-sans '>
      <h4 className='lg:visible md:invisible sm:invisible text-xl p-2 mx-4 font-semibold'>Recommended</h4>
       <div className='grid lg:grid-cols-4 lg:gap-4 lg:w-[1400px] lg:ml-10 md:grid-cols-3 md1:grid-cols-4 md:gap-5 md1:gap-5 md:ml-20 md:w-[800px] md1:w-[940px] md1:ml-14 sm:w-[450px] sm:mx-6 '>
      
      {VideoDatas.map((VideoData,index) => 
    (<div onMouseEnter={(e) => HandlMouseEnter(e,index)} onMouseLeave={(e) => HandlMouseLeave(e,index)} className=' hover:scale-125 hover:shadow-lg lg:w-auto md:w-auto md1:w-auto  py-2 mb-4' key={VideoData.id}>
      <Link href={`/VideoDatas/${VideoData.id}`}>
      <img className='pb-2 ' alt='thambnails' src={VideoData.thambnails}></img> 
      </Link>
      <div className='flex items-center justify-between'>
          <img className='w-8 h-7 items-start  rounded-full mr-1' src={VideoData.chaineAvatar}></img>
          <h2 className=' text-md m-1 font-semibold'>{VideoData.title}</h2>
          <svg className=" ml-1 w-4 h-4 hover:visible cursor-pointer" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M10 6a2 2 0 110-4 2 2 0 010 4zM10 12a2 2 0 110-4 2 2 0 010 4zM10 18a2 2 0 110-4 2 2 0 010 4z"></path></svg>
        </div>
       <div>
          <h2 className=' text-sm text-gray-600 mx-9'>{VideoData.chaineName}</h2>
          <h4 className=' text-sm text-gray-500 mx-9'>{VideoData.Views} . {VideoData.DateUpload}</h4>
       </div>
       {/* Video Infos Watch later and add to Queue */}
       {<div ref={VideoInfs} id={'VideoData1'+ index} className='grid grid-cols-2 gap-4 p-1 m-2 invisible bg-white'>
          <div className=' flex items-center bg-stone-200 lg:p-1 md:p-0 border  text-stone-500 '>
              <svg className=' lg:mx-2 md:mx-1 sm:mx-2' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock-history" viewBox="0 0 16 16"><path d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z"/><path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z"/><path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/></svg>
              <button className=' lg:text-base md:text-sm'>Watch Later</button>
          </div>

          <div className='flex items-center bg-stone-200 p-1 border  text-stone-500'>
              <svg className='lg:mx-2 md:mx-1 sm:mx-2 ' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-collection-play" viewBox="0 0 16 16"><path d="M2 3a.5.5 0 0 0 .5.5h11a.5.5 0 0 0 0-1h-11A.5.5 0 0 0 2 3zm2-2a.5.5 0 0 0 .5.5h7a.5.5 0 0 0 0-1h-7A.5.5 0 0 0 4 1zm2.765 5.576A.5.5 0 0 0 6 7v5a.5.5 0 0 0 .765.424l4-2.5a.5.5 0 0 0 0-.848l-4-2.5z"/> <path d="M1.5 14.5A1.5 1.5 0 0 1 0 13V6a1.5 1.5 0 0 1 1.5-1.5h13A1.5 1.5 0 0 1 16 6v7a1.5 1.5 0 0 1-1.5 1.5h-13zm13-1a.5.5 0 0 0 .5-.5V6a.5.5 0 0 0-.5-.5h-13A.5.5 0 0 0 1 6v7a.5.5 0 0 0 .5.5h13z"/></svg>
              <button className='lg:text-base md:text-sm '>Add to Queue</button>
          </div>
        </div>}
    </div>)
    )}
    </div>
       <hr  className=' border-t-4 mx-8 py-1'/>
      <div className='py-6 mx-6  flex items-center  '>
        <img className='w-6 h-6' src='./shorts-red.png'></img>
        <h4 className=' text-xl p-2 font-semibold'>Shorts</h4>
      </div>
      <div className='grid lg:grid-cols-8 md:grid-cols-4 sm1:grid-cols-2 sm:grid-cols-1 gap-4 md:ml-10 lg:ml-0 sm:ml-8'>
            {ShortDatas.map( (ShortData) => 
              <div className='preview-shorts lg:mx-1  lg:p-2 lg:pb-4  md:w-auto ' key={ShortData.id}>
              
                  <Link className=' grid p-1' href={`/Shorts/${ShortData.id}`}>
                  <ReactPlayer className=""
                                            url={ShortData.url}
                                            height="300px"
                                            width="auto"
                                            controls
                                            /> 
                                            </Link>
                  <h2 className=' text-md my-1'>{ShortData.title}</h2>
                  <h4 className=' text-md text-gray-500 mx-2'>{ShortData.Views}</h4>
                  
              
              </div>
              )}
              {ShortDatas.map( (ShortData) => 
              <div className='preview-shorts lg:mx-1  lg:p-2 lg:pb-4  md:w-auto ' key={ShortData.id}>
              
                  <Link className=' grid p-1' href={`/Shorts/${ShortData.id}`}>
                  <ReactPlayer className=""
                                            url={ShortData.url}
                                            height="300px"
                                            width="auto"
                                            controls
                                            /> 
                                            </Link>
                  <h2 className=' text-md my-1'>{ShortData.title}</h2>
                  <h4 className=' text-md text-gray-500 mx-2'>{ShortData.Views}</h4>
                  
              
              </div>
              )}
              
        </div>
        <hr  className=' border-t-4 pb-4 mx-8 lg:visible md:invisible sm:invisible'/>
        <div className='grid lg:grid-cols-4 lg:gap-4 lg:w-[1400px] lg:ml-10 md:grid-cols-3 md1:grid-cols-4 md:gap-5 md1:gap-5 md:ml-20 md:w-[800px] md1:w-[940px] md1:ml-14 sm:w-[450px] sm:mx-6 '>
      
      {VideoDatas.map((VideoData,index) => 
    (<div onMouseEnter={(e) => HandlMouseEnter(e,index)} onMouseLeave={(e) => HandlMouseLeave(e,index)} className=' hover:scale-125 hover:shadow-lg lg:w-auto md:w-auto md1:w-auto  py-2 mb-4' key={VideoData.id}>
      <Link href={`/VideoDatas/${VideoData.id}`}>
      <img className='pb-2 ' alt='thambnails' src={VideoData.thambnails}></img> 
      </Link>
      <div className='flex items-center justify-between'>
          <img className='w-8 h-7 items-start  rounded-full mr-1' src={VideoData.chaineAvatar}></img>
          <h2 className=' text-md m-1 font-semibold'>{VideoData.title}</h2>
          <svg className=" ml-1 w-4 h-4 hover:visible cursor-pointer" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M10 6a2 2 0 110-4 2 2 0 010 4zM10 12a2 2 0 110-4 2 2 0 010 4zM10 18a2 2 0 110-4 2 2 0 010 4z"></path></svg>
        </div>
       <div>
          <h2 className=' text-sm text-gray-600 mx-9'>{VideoData.chaineName}</h2>
          <h4 className=' text-sm text-gray-500 mx-9'>{VideoData.Views} . {VideoData.DateUpload}</h4>
       </div>
       {/* Video Infos Watch later and add to Queue */}
       { showinfos && <div id={'VideoData1'+ index} className='grid grid-cols-2 gap-4 p-1 m-2 invisible bg-white'>
          <div className=' flex items-center bg-stone-200 lg:p-1 md:p-0 border  text-stone-500 '>
              <svg className=' lg:mx-2 md:mx-1 sm:mx-2' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock-history" viewBox="0 0 16 16"><path d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z"/><path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z"/><path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/></svg>
              <button className=' lg:text-base md:text-sm'>Watch Later</button>
          </div>

          <div className='flex items-center bg-stone-200 p-1 border  text-stone-500'>
              <svg className='lg:mx-2 md:mx-1 sm:mx-2 ' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-collection-play" viewBox="0 0 16 16"><path d="M2 3a.5.5 0 0 0 .5.5h11a.5.5 0 0 0 0-1h-11A.5.5 0 0 0 2 3zm2-2a.5.5 0 0 0 .5.5h7a.5.5 0 0 0 0-1h-7A.5.5 0 0 0 4 1zm2.765 5.576A.5.5 0 0 0 6 7v5a.5.5 0 0 0 .765.424l4-2.5a.5.5 0 0 0 0-.848l-4-2.5z"/> <path d="M1.5 14.5A1.5 1.5 0 0 1 0 13V6a1.5 1.5 0 0 1 1.5-1.5h13A1.5 1.5 0 0 1 16 6v7a1.5 1.5 0 0 1-1.5 1.5h-13zm13-1a.5.5 0 0 0 .5-.5V6a.5.5 0 0 0-.5-.5h-13A.5.5 0 0 0 1 6v7a.5.5 0 0 0 .5.5h13z"/></svg>
              <button className='lg:text-base md:text-sm '>Add to Queue</button>
          </div>
        </div>}
    </div>)
    )}
    </div>
      
    </div>
  );
}

export default Contenus;