import React, { useState } from 'react';
import Link from 'next/link';
import {Notifications, Languages,Countries} from './data';
function Navbar() {
  
 
  
  /* export const Notification = [
    {
      "id": 1,
      "ChaineName" : "ZAK Official" ,  
      "VideoAvatar": "https://www.youtube.com/channel/UCw_MDHG1l88aKxAt1rzRDDg",
      "VideoURL":"https://www.youtube.com/watch?v=k-VBUVz5jXM",
      "VideoTitle":"AFRO DEEP HOUSE MIX | black coffee | shimza | caiiro",
      "VideoDate":"7 hours",
      "VideoThambnail":"http://i3.ytimg.com/vi/k-VBUVz5jXM/hqdefault.jpg",
*/

  const [showProfil,setShowProfil] = useState(false);
  const [showBelt,setShowBelt] = useState(false);
  const [showAddv,setShowAddv] = useState(false);
  const [shownotif,setNotif] = useState(true);
  const [showProfilAccount,setShowProfilAccount] = useState(false);
  const [showdotsNotification,setShowdotsNotification] = useState(false);
  const [showAppearance,setShowAppearance] = useState(false);
  const [showLanguage,setShowLanguage] = useState(false);
  const [showRestricted,setShowRestricted] = useState(false);
  const [showLocation,setShowLocation] = useState(false);
  //const [] = useState();

  const [showsearch,setShowSearch] = useState(false);

  let cnt =  0 ;
  const Showsearchbar = () => {
    setShowSearch(!showsearch);

  }
  const HandlShowLocation = () => {
      setShowLocation(true);
      setShowProfil(false);
  }
  const HandlShowAppearance =() => {
    setShowAppearance(true);
    setShowProfilAccount(false);
    setShowProfil(false);
  }
  const HandlShowLang = () => {
    setShowLanguage(true);
    setShowProfil(false);
  }
  const HandlRestrictedMode = () => {
    setShowRestricted(true);
    setShowProfil(false);
  }
  const Beltfunc = () => {
          setShowBelt(!showBelt);
          cnt= cnt+1;
          if(cnt > 0)
          {
            setNotif(!shownotif);
          }
  }
    const AlertMicro = () => {
      alert('Get Micro Permission');
    }
    const HandlRetourToProfilMenu = () => {
        setShowProfilAccount(false);
        setShowProfil(true);
        setShowAppearance(false);
        setShowLanguage(false);
        setShowRestricted(false);
        setShowLocation(false);
    }
    const HandlSwitchAccount = () => {
        setShowProfilAccount(true);
        setShowProfil(false);
    }
    const HandlEnterNotifDots = (e,index) => {
      const dotsShow = document.getElementById('dotShow'+ index);
      dotsShow.style.visibility = 'visible';
    }
    const HandlLeaveNotifDots = (e,index) => {
      const dotsShow = document.getElementById('dotShow'+ index);
      dotsShow.style.visibility = 'hidden';
    }
    const HandlmouseEnterNotificationOption = (e,index) => {
      //setshowdotsNotification(true);
       //const divshow1 = document.getElementById('target'+ index);
       //divshow1.style.visibility = 'visible';
    }
    const HandlmouseLeaveNotificationOption = (e,index) => {
      //setshowdotsNotification(false);
       // const divshow1 = document.getElementById('target'+ index);
       // divshow1.style.visibility = 'hidden';
    }
    // first give an id for the target example id={target,index}
    //onMouseEnter={(e) => HandlMouseEnter(e,index)} onMouseLeave={(e) => HandlMouseLeave(e,index)} 
    // const HandlMouseEnter = (e,index) => {
    //   // console.log("target",e.target);
  
    //   setTimeout(() => {
    //     setShowInfos(true);
    //     //setDivShowd(index);
    //     const divshow1 = document.getElementById('VideoData'+ index);
    //     divshow1.style.visibility = 'visible';
    //     const divshow2 = document.getElementById('VideoData1'+ index);
    //     divshow2.style.visibility = 'visible';
    //   },100);
      
    // }
    // const HandlMouseLeave = (e,index) => {
    //   //setShowInfos(false);
    //   //setDivShowd(-1);
    //     const divshow1 = document.getElementById('VideoData'+ index);
    //     divshow1.style.visibility = 'hidden';
    //     const divshow2 = document.getElementById('VideoData1'+ index);
    //     divshow2.style.visibility = 'hidden';
  
    // }
  // const notifnbr = document.querySelector('#nbrNotification');
  // const hider = () => {
        
        
  // }
  // const btnprofile = document.querySelector("#profil");
  // const profildropdown = document.querySelector("#profildropdown");

    
  //  const afterClick = () => {
  //   // alert("welcome");
  //     btnprofile.addEventListener('click', () => {

  //     if(profildropdown.classList.countains('hidden'))
  //     {
  //       profildropdown.classList.remove('hidden');
  
  //     }else
  //     {
  //       profildropdown.classList.add('hidden');
  //     }
  //   })
  //  }
  //  document.getElementById("").addEventListener("click",afterClick)
  return (
    <div className="ytblogo flex justify-between items-center h-14  px-6 fixed top-0 bg-white lg:w-screen md1:w-[1200px] md:w-[1070px]  ">
        <div className=" menu flex justify-between items-center bg-white">
            <svg id='hambrgr' className="w-6 h-6  mx-0 cursor-pointer" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"></path></svg>
            <Link href='/'>
               <img className=" w-32 mx-1 px-1 cursor-pointer" src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSJjdtC2Iy1QHnse2omqaDHTXTx7cpoYzYV9Zm_O2fRjzkICtrx4x8j6g7_Ep7jZoWojw&usqp=CAU'></img> 
            </Link> 
        </div>
        
        {/* Search Code */}
        <div className="search flex items-center ">
           <div className=' flex justify-between items-center '> 
              <input className=' lg:w-[500px] px-2 md:w-38 md:w-[300px] md:visible sm:invisible sm:w-1 lg:border md:border-2 h-10 focus:outline-none focus:border-blue-500 ' type="text" placeholder='Search' />
              <div className='px-3 md:border lg:border h-10 w-15 lg:bg-slate-100 md:bg-slate-100 '>
                <svg onClick={Showsearchbar} className="w-6 m-2 cursor-pointer  border-l-0 items-center" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
              </div>
            </div>
            <div className=' mx-2  p-2 rounded-full bg-stone-100  '>
               <img onClick={AlertMicro} className='w-5 cursor-pointer' src='micro.png'></img>
              {/* <svg className=" cursor-pointer w-4 h-4  m-3  hover:scale-110" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 11a7 7 0 01-7 7m0 0a7 7 0 01-7-7m7 7v4m0 0H8m4 0h4m-4-8a3 3 0 01-3-3V5a3 3 0 116 0v6a3 3 0 01-3 3z"></path></svg> */}
            </div>
        </div>
        <div className='setting px-1 flex justify-between items-center'>
            <div className='' onClick={() => setShowAddv(!showAddv)}>
                <svg  className="w-6 h-6 mx-2 cursor-pointer" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 10l4.553-2.276A1 1 0 0121 8.618v6.764a1 1 0 01-1.447.894L15 14M5 18h8a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v8a2 2 0 002 2z"></path></svg>
            </div>
            <div onClick={Beltfunc}  className="mx-3 ">
                { shownotif && 
                  <div id='nbrNotification' className='badge absolute top-0 ml-2 p-1 mt-1 bg-red-600 text-white text-xs uppercase  rounded-xl cursor-pointer'>
                    <span className='px-1 text-[10px] font-semibold'>9+</span>
                  </div>
                }
                <svg  className="w-6 h-6 cursor-pointer" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"></path></svg>
            </div>
            <div className='ml-2'>
                <div onClick={() => setShowProfil(!showProfil)} className='' >
                  <img  className="w-9 rounded-full hover:scale-110 mx-2 cursor-pointer" id='profil' src='https://yt3.ggpht.com/yti/AJo0G0msRdlewXnnmLuEnR5KATOSgyT3MG6B-5fIFCxZ=s88-c-k-c0x00ffffff-no-rj-mo'></img>
                </div>
                {/* Search bar in mobile phones */}
                {
                  showsearch && <div className=' lg:invisible md:invisible sm:visible'>
                    <div  className='border rounded-lg bg-slate-100'>
                      <div class="absolute right-32 mt-3  origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
                         <input className='px-2 py-2 w-[300px]' type="text" placeholder='search' />
                      </div>
                    </div>
                  </div>}
                {/* Upload Video */}
                { showAddv && <div  className='border bg-slate-100'>
                         <div class="absolute right-36 mt-2 w-[220px] origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
                         <div className="py-2">
                                <div className='flex items-center ml-5 hover:bg-stone-100'>
                                  <svg className='w-5 h-5' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-play-btn" viewBox="0 0 16 16"><path d="M6.79 5.093A.5.5 0 0 0 6 5.5v5a.5.5 0 0 0 .79.407l3.5-2.5a.5.5 0 0 0 0-.814l-3.5-2.5z"/><path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm15 0a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z"/></svg>
                                  <a href="#" className="block px-4 py-2 text-base " id="menu-item-0">Upload video</a>
                                </div>

                                <div className='flex items-center ml-5 hover:bg-stone-100'>
                                      <img className='w-5 h-5' src='./live.png'></img>
                                      <a href="#" className="block px-4 py-2 text-base" id="menu-item-1">Go live</a>

                                </div>

                          
                    </div>
                    </div>
                  </div>}
                {/* dropdown Belt notification */}
                {showBelt &&  <div id='profildropdown' className='border bg-slate-100'>
                         <div class="absolute right-24 mt-2 w-[500px] origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
                             <div className=' flex justify-between items-center py-3'>
                                <h4 className='ml-3'>Notifications</h4>
                                <Link href="www.youtube.com/account_notifications">
                                  <svg className="w-6 h-6 mr-3 cursor-pointer" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"></path><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path></svg>
                                </Link>
                             </div>
                             <hr/> 
                               <div className=' h-[500px] overflow-y-scroll'>
                                 { Notifications.map((Notif,index) => (
                                  <div  onMouseEnter={(e) => HandlEnterNotifDots(e,index)} onMouseLeave={(e) => HandlLeaveNotifDots(e,index)} className='py-2 flex items-center justify-between hover:bg-stone-100' key={Notif.id}>
                                      <div className=' flex items-center '>
                                          <div className='flex items-center text-center mr-6'>
                                            <h4 className=' text-blue-500 font-bold text-2xl ml-2'>.</h4>
                                            <img className='w-12 h-12 mx-1 mr-2 border rounded-full ' src={Notif.VideoAvatar}></img>
                                          </div>
                                          <div className=' flex flex-col w-[240px]'>
                                            <h4>{Notif.VideoTitle}</h4>
                                            <p className=' text-sm text-gray-500'>{Notif.VideoDate}</p>
                                          </div>
                                      </div>
                                      <img className='w-24 h-26 ml-2 mr-2 ' src={Notif.VideoThambnail}></img>
                                        <div>
                                        { 
                                        <div id={'dotShow'+ index}  className=' invisible'>
                                          <svg id='dots' className="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 5v.01M12 12v.01M12 19v.01M12 6a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2z"></path></svg>
                                        </div>
                                        }
                                        </div>
                                  </div> 
                                 ))}
                                 
                                  
                              </div>
                         </div>
                 </div> }
                 {/* Appearance Theme */}
                {showAppearance && <div className='border rounded-lg bg-slate-100 font-[Roboto]'>
                      <div class="absolute right-10 mt-3  origin-top-right divide-y divide-gray-100 bg-white rounded-sm  ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
                         <div onClick={HandlRetourToProfilMenu} className='flex items-center px-2 py-3 w-[300px] cursor-pointer'>
                            <svg className="w-6 h-6 mx-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18"></path></svg>
                            <h4 className=''>Appearance</h4>
                         </div>
                         <hr/>
                         
                         <div className=' px-2 py-2 w-[300px]'>
                         <div className='py-1'>
                          <p className=' text-sm text-gray-500 ml-2'>Setting applies to this browser only</p>
                         </div>
                             <div className=' py-1'>
                                <div className=' flex items-center py-2 hover:bg-stone-100  cursor-pointer'>
                                  <svg className="w-6 h-6 mx-3"/>
                                  <h4 className=' text-md '>Use device theme</h4>
                                </div>
                                <div className=' flex items-center py-2 hover:bg-stone-100  cursor-pointer'>
                                <svg className="w-6 h-6 mx-3"/>
                                <h4 className=' text-md '>Dark theme</h4>
                                </div>
                                <div className='flex items-center py-2 hover:bg-stone-100  cursor-pointer'>
                                  <svg className="w-6 h-6 mx-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7"></path></svg>
                                  <h4 className=' text-md'>Light theme</h4>
                                </div>
                             </div>
                         </div>
                      </div>
                  </div>}
                  {/* Localisation location */}
                  {
                   showLocation && <div className='border rounded-lg bg-slate-100 font-[Roboto]'>
                      <div class="absolute right-20 top-1 mt-0   origin-top-right divide-y divide-gray-100 bg-white rounded-sm  ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
                        <div onClick={HandlRetourToProfilMenu} className='flex items-center px-2 py-3 w-[300px] cursor-pointer'>
                           <svg className="w-6 h-6 mx-3 text-base" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18"></path></svg>
                           <h4 className=''>Choose your location</h4>
                        </div>
                        <div className='py-2 px-1 h-screen overflow-y-scroll'>
                          { Countries.map((Countrie) => (
                            <div className='w-[300px] p-2 cursor-pointer hover:bg-stone-100' key={Countrie.code}>
                                  <h4 className=' text-md ml-8'>{Countrie.name}</h4>
                            </div>
                          ))}
                        </div>
                      </div>
                    </div>
                  }
                  {/* Restricred Mode */}
                  { showRestricted && <div className='border rounded-lg bg-slate-100 font-[Roboto]'>
                      <div class="absolute right-8 mt-0   origin-top-right divide-y divide-gray-100 bg-white rounded-sm  ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
                         <div className='flex items-center px-2 py-3 w-[300px] cursor-pointer'>
                            <svg onClick={HandlRetourToProfilMenu} className="w-6 h-6 cursor-pointer mx-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18"></path></svg>
                             <h4 className=''>Restricted Mode</h4>
                         </div>
                         <div>
                            <div className=''>
                              <p className='p-2 w-[290px] m-2 text-md'>This helps hide potentially mature videos. No filter is 100% accurate.</p>
                            </div>
                            <div className=''>
                              <h4 className='p-2 w-[290px] m-2 text-md'>This setting only applies to this browser.</h4>
                            </div>
                            <div className='form-check form-switch flex items-center p-2 ' >
                              <h4>ACTIVATE RESTRICTED MODE</h4>
                             <div className=' flex items-center justify-center'>
                                <input id="toggle-switch" className=" appearance-none w-9 ml-2 rounded-full h-4 border border-blue-300   bg-gray-200  checked:bg-gray-400  cursor-pointer transition duration-200 relative" type="checkbox" role="switch"  />
                             </div>
                            </div>
                         </div>
                         
                      </div>
                  </div> }      
                  {/* Handle Language switch */}
                  {showLanguage && <div className='border rounded-lg bg-slate-100 font-[Roboto]'>
                      <div class="absolute right-20 top-1 mt-0  origin-top-right divide-y divide-gray-100 bg-white rounded-sm  ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
                         <div onClick={HandlRetourToProfilMenu} className='flex items-center px-2 py-3 w-[300px] cursor-pointer'>
                           <svg className="w-6 h-6  mx-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18"></path></svg>
                           <h4 className=''>Choose your language</h4>
                         </div>
                         <div className='px-2 py-2 w-[300px] h-screen overflow-y-scroll cursor-pointer'>
                              {Languages.map((Language) => (
                                <div className='py-0 ' key={Language.id}>

                                      <div className='hover:bg-stone-100 py-2'>
                                        <h4 className='ml-10 text-md'>{Language.value}</h4>
                                      </div>
                                </div>
                              ))}
                              {Languages.map((Language) => (
                                <div className='py-0 ' key={Language.id}>

                                      <div className='hover:bg-stone-100 py-2'>
                                        <h4 className='ml-10 text-md'>{Language.value}</h4>
                                      </div>
                                </div>
                              ))}
                         </div>
                      </div>
                  </div>}
                 {/* Handl Profile Switch Account */}
                 { showProfilAccount && <div className=''>
                    <div className='border rounded-lg bg-slate-100 font-[Roboto]'>
                      <div class="absolute right-10 mt-3  origin-top-right divide-y divide-gray-100 bg-white rounded-sm  ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
                         <div className='flex items-center px-2 py-2 w-[300px]'>
                            <svg onClick={HandlRetourToProfilMenu} className=" ml-2 w-6 h-6 cursor-pointer text-gray-600" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18"></path></svg>
                            <h1 className='ml-2 text-base py-2'>Accounts</h1>
                         </div>
                         <div className='px-2 py-2 w-[300px] cursor-pointer'>
                              <h4 className='ml-2 text-sm'>Taha Yacine Salim</h4>
                              <h4 className=' ml-2 text-sm'>salimtahayacine@gmail.com</h4>
                         </div>
                         <div className='py-2 w-[300px]'>
                              <div className='py-1 px-3 flex items-center hover:bg-stone-100 cursor-pointer'>
                                  <img className='w-12 h-12' src='./avatar1.svg'></img>
                                  <div className='ml-2'>
                                    <h4 className=' text-md'>Chaine Name</h4>
                                    <h6 className=' text-sm'>1,535 subscribers</h6>
                                  </div>
                              </div>
                              <div className='py-1 px-3 flex items-center hover:bg-stone-100 cursor-pointer'>
                              <img className='w-12 h-12' src='./avatar2.svg'></img>
                                  <div className='ml-2'>
                                    <h4 className=' text-md'>Chaine Name</h4>
                                    <h6 className=' text-sm'>No Subscribers</h6>
                                  </div>
                              </div>
                              <div className='py-1 px-3 flex items-center justify-between hover:bg-stone-100 cursor-pointer'>
                                  <div className='flex items-center'>
                                     <img className='w-12 h-12  rounded-full focus:outline ring-2 ring-blue-600' src='https://yt3.ggpht.com/yti/AJo0G0msRdlewXnnmLuEnR5KATOSgyT3MG6B-5fIFCxZ=s108-c-k-c0x00ffffff-no-rj'></img>
                                     <div className='ml-2'>
                                       <h4 className=' text-md'>OKPain</h4>
                                       <h6 className=' text-sm'>50 subscribers</h6>
                                     </div>
                                  </div>
                                  <svg className="w-7 h-7 mr-2 text-stone-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7"></path></svg>
                              </div>
                              < div className='hover:bg-stone-100 cursor-pointer my-2'>
                                 <h4 className='py-2 ml-3 '>View all channels</h4>
                              </div>
                         </div>
                           <div className='py-1'>
                                <div className=' flex items-center py-3 hover:bg-stone-100 cursor-pointer'>
                                      <svg className=' w-6 h-6 mx-5' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-plus" viewBox="0 0 16 16"><path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/><path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/></svg>
                                      <h4 className=' ml-2 text-md'>Add account</h4>
                                </div>
                                <div className='flex items-center py-3 hover:bg-stone-100 cursor-pointer'>
                                      <svg className=' w-6 h-6 mx-4' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-box-arrow-in-right" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0v-2z"/><path fill-rule="evenodd" d="M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/></svg>
                                      <h4 className=' ml-4 text-md'>Sign out</h4>
                                </div>
                           </div>
                      </div>
                    </div>
                  </div>}
                    {/* Dropdown profil */}
                      {showProfil && <div id='profildropdown' className='border bg-slate-100 font-[Roboto]'>
                         <div class="absolute right-4 mt-2 w-[300px] origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
                        <div className="py-2 flex items-center">
                      {/* Active: "bg-gray-100 text-gray-900", Not Active: "text-gray-700" */}
                        <div className=''>
                           <img className="w-10 h-10 rounded-full hover:scale-110 mx-2 cursor-pointer" src='https://yt3.ggpht.com/yti/AJo0G0msRdlewXnnmLuEnR5KATOSgyT3MG6B-5fIFCxZ=s88-c-k-c0x00ffffff-no-rj-mo'></img>
                        </div>
                        <div>
                          <a href="#" className="text-gray-700 block px-4 py-2 text-base font-semibold" id="menu-item-0">Chaine Name</a>
                          <a href="#" className="text-blue-500 block px-4 py-2 text-base" id="menu-item-1">Manage your Google Account</a>
                        </div>
                    </div>
                    <div className="py-1" role="none">
                      <div className='flex items-center ml-5 hover:bg-stone-100'>
                        <svg className='w-5 h-5 ml-4' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-square" viewBox="0 0 16 16"><path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/><path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm12 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1v-1c0-1-1-4-6-4s-6 3-6 4v1a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12z"/></svg>
                        <a href="#" className="text-gray-700 block px-4 py-2 text-base" role="menuitem" tabindex="-1" id="menu-item-2">Your channel</a>
                      </div>
                      <div className='flex items-center ml-5 hover:bg-stone-100'>
                        <img className='w-5 h-5 ' src='./youtube-studio.png'></img>
                        <a href="#" className="text-gray-700 block px-4 py-2 text-base" role="menuitem" tabindex="-1" id="menu-item-3">Youtube Studio</a>
                      </div>
                      <div onClick={HandlSwitchAccount} className='flex items-center justify-between ml-5 hover:bg-stone-100 cursor-pointer'>
                         <div className='flex items-center '>
                           <svg className='w-5 h-5' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-lines-fill" viewBox="0 0 16 16"><path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-5 6s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zM11 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5zm.5 2.5a.5.5 0 0 0 0 1h4a.5.5 0 0 0 0-1h-4zm2 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2zm0 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2z"/></svg>
                           <a href="#" className="text-gray-700 block px-4 py-2 text-base" role="menuitem" tabindex="-1" id="menu-item-2">Switch account</a>
                         </div>
                         <svg class="w-6 h-6 mr-2 cursor-pointer " fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>
                      </div>
                      <div className='flex items-center ml-4 hover:bg-stone-100'>
                        <svg className='w-5 h-5' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-box-arrow-in-right" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0v-2z"/><path fill-rule="evenodd" d="M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/></svg>
                        <a href="#" className="text-gray-700 block px-4 py-2 text-base" role="menuitem" tabindex="-1" id="menu-item-3">Sign out</a>
                      </div> 
                    </div>
                    <div className="py-1" role="none">
                      <div className='flex items-center ml-5 hover:bg-stone-100' >
                      <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8c-1.657 0-3 .895-3 2s1.343 2 3 2 3 .895 3 2-1.343 2-3 2m0-8c1.11 0 2.08.402 2.599 1M12 8V7m0 1v8m0 0v1m0-1c-1.11 0-2.08-.402-2.599-1M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                      <a href="#" className="text-gray-700 block px-4 py-2 text-base" role="menuitem" tabindex="-1" id="menu-item-4">Purchase and memberships</a>
                      </div>
                      <div className='flex items-center ml-5 hover:bg-stone-100'>
                         <svg className='w-5 h-5 ' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clipboard-data" viewBox="0 0 16 16"><path d="M4 11a1 1 0 1 1 2 0v1a1 1 0 1 1-2 0v-1zm6-4a1 1 0 1 1 2 0v5a1 1 0 1 1-2 0V7zM7 9a1 1 0 0 1 2 0v3a1 1 0 1 1-2 0V9z"/><path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z"/><path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z"/></svg>
                         <a href="#" className="text-gray-700 block px-4 py-2 text-base" role="menuitem" tabindex="-1" id="menu-item-5">Your data in Youtube</a>
                      </div>
                    </div>
                    <div className="py-1" role="none">
                      <div onClick={HandlShowAppearance} className='flex items-center justify-between ml-5 hover:bg-stone-100 cursor-pointer'>
                         <div  className='flex items-center'>
                           <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.354 15.354A9 9 0 018.646 3.646 9.003 9.003 0 0012 21a9.003 9.003 0 008.354-5.646z"></path></svg>
                           <a href="#" className="text-gray-700 block px-4 py-2 text-base" role="menuitem" tabindex="-1" id="menu-item-4">Appearance: Light</a>
                         </div>
                         <svg className="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>
                      </div>
                     
                      <div onClick={HandlShowLang} className='flex items-center justify-between ml-5 hover:bg-stone-100 cursor-pointer'>
                         <div className='flex items-center'>
                           <svg className='w-5 h-5' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-translate" viewBox="0 0 16 16"><path d="M4.545 6.714 4.11 8H3l1.862-5h1.284L8 8H6.833l-.435-1.286H4.545zm1.634-.736L5.5 3.956h-.049l-.679 2.022H6.18z"/><path d="M0 2a2 2 0 0 1 2-2h7a2 2 0 0 1 2 2v3h3a2 2 0 0 1 2 2v7a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2v-3H2a2 2 0 0 1-2-2V2zm2-1a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H2zm7.138 9.995c.193.301.402.583.63.846-.748.575-1.673 1.001-2.768 1.292.178.217.451.635.555.867 1.125-.359 2.08-.844 2.886-1.494.777.665 1.739 1.165 2.93 1.472.133-.254.414-.673.629-.89-1.125-.253-2.057-.694-2.82-1.284.681-.747 1.222-1.651 1.621-2.757H14V8h-3v1.047h.765c-.318.844-.74 1.546-1.272 2.13a6.066 6.066 0 0 1-.415-.492 1.988 1.988 0 0 1-.94.31z"/></svg>
                           <a href="#" className="text-gray-700 block px-4 py-2 text-base" role="menuitem" tabindex="-1" id="menu-item-5">Language: English</a>    
                         </div>
                         <svg className="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>

                      </div>
                      <div onClick={HandlRestrictedMode} className='flex items-center justify-between ml-5 hover:bg-stone-100 cursor-pointer'>
                        <div className='flex items-center '>
                          <svg className='w-5 h-5' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-shield-check" viewBox="0 0 16 16"><path d="M5.338 1.59a61.44 61.44 0 0 0-2.837.856.481.481 0 0 0-.328.39c-.554 4.157.726 7.19 2.253 9.188a10.725 10.725 0 0 0 2.287 2.233c.346.244.652.42.893.533.12.057.218.095.293.118a.55.55 0 0 0 .101.025.615.615 0 0 0 .1-.025c.076-.023.174-.061.294-.118.24-.113.547-.29.893-.533a10.726 10.726 0 0 0 2.287-2.233c1.527-1.997 2.807-5.031 2.253-9.188a.48.48 0 0 0-.328-.39c-.651-.213-1.75-.56-2.837-.855C9.552 1.29 8.531 1.067 8 1.067c-.53 0-1.552.223-2.662.524zM5.072.56C6.157.265 7.31 0 8 0s1.843.265 2.928.56c1.11.3 2.229.655 2.887.87a1.54 1.54 0 0 1 1.044 1.262c.596 4.477-.787 7.795-2.465 9.99a11.775 11.775 0 0 1-2.517 2.453 7.159 7.159 0 0 1-1.048.625c-.28.132-.581.24-.829.24s-.548-.108-.829-.24a7.158 7.158 0 0 1-1.048-.625 11.777 11.777 0 0 1-2.517-2.453C1.928 10.487.545 7.169 1.141 2.692A1.54 1.54 0 0 1 2.185 1.43 62.456 62.456 0 0 1 5.072.56z"/><path d="M10.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z"/></svg>
                          <a href="#" className="text-gray-700 block px-4 py-2 text-base" role="menuitem" tabindex="-1" id="menu-item-4">Restricted Mode:Off</a>
                        </div>
                        <svg className="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>
                      </div>
                      <div onClick={HandlShowLocation} className='flex items-center justify-between ml-5 hover:bg-stone-100 cursor-pointer' >
                        <div className='flex items-center'>
                         <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9"></path></svg>
                         <a href="#" className="text-gray-700 block px-4 py-2 text-base" role="menuitem" tabindex="-1" id="menu-item-5">Location: US-Canada</a>
                        </div>
                        <svg className="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>
                      </div>
                     
                      <div className='flex items-center ml-5 hover:bg-stone-100'>
                         <svg className='w-5 h-5' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-keyboard" viewBox="0 0 16 16"><path d="M14 5a1 1 0 0 1 1 1v5a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h12zM2 4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2H2z"/><path d="M13 10.25a.25.25 0 0 1 .25-.25h.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-.5a.25.25 0 0 1-.25-.25v-.5zm0-2a.25.25 0 0 1 .25-.25h.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-.5a.25.25 0 0 1-.25-.25v-.5zm-5 0A.25.25 0 0 1 8.25 8h.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-.5A.25.25 0 0 1 8 8.75v-.5zm2 0a.25.25 0 0 1 .25-.25h1.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-1.5a.25.25 0 0 1-.25-.25v-.5zm1 2a.25.25 0 0 1 .25-.25h.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-.5a.25.25 0 0 1-.25-.25v-.5zm-5-2A.25.25 0 0 1 6.25 8h.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-.5A.25.25 0 0 1 6 8.75v-.5zm-2 0A.25.25 0 0 1 4.25 8h.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-.5A.25.25 0 0 1 4 8.75v-.5zm-2 0A.25.25 0 0 1 2.25 8h.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-.5A.25.25 0 0 1 2 8.75v-.5zm11-2a.25.25 0 0 1 .25-.25h.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-.5a.25.25 0 0 1-.25-.25v-.5zm-2 0a.25.25 0 0 1 .25-.25h.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-.5a.25.25 0 0 1-.25-.25v-.5zm-2 0A.25.25 0 0 1 9.25 6h.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-.5A.25.25 0 0 1 9 6.75v-.5zm-2 0A.25.25 0 0 1 7.25 6h.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-.5A.25.25 0 0 1 7 6.75v-.5zm-2 0A.25.25 0 0 1 5.25 6h.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-.5A.25.25 0 0 1 5 6.75v-.5zm-3 0A.25.25 0 0 1 2.25 6h1.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-1.5A.25.25 0 0 1 2 6.75v-.5zm0 4a.25.25 0 0 1 .25-.25h.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-.5a.25.25 0 0 1-.25-.25v-.5zm2 0a.25.25 0 0 1 .25-.25h5.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-5.5a.25.25 0 0 1-.25-.25v-.5z"/></svg>
                         <a href="#" className="text-gray-700 block px-4 py-2 text-base" role="menuitem" tabindex="-1" id="menu-item-4">Keyboard shortcuts</a>
                      </div>
                         
                    </div>
                    <div className="py-1 flex items-center ml-5 hover:bg-stone-100" role="none">
                      <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"></path><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path></svg>
                      <a href="#" className="text-gray-700 block px-4 py-2 text-base" role="menuitem" tabindex="-1" id="menu-item-6">Settings</a>
                    </div>
                    <div className="py-1" role="none">
                      <div className='flex items-center ml-5 hover:bg-stone-100'>
                        <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8.228 9c.549-1.165 2.03-2 3.772-2 2.21 0 4 1.343 4 3 0 1.4-1.278 2.575-3.006 2.907-.542.104-.994.54-.994 1.093m0 3h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                        <a href="#" className="text-gray-700 block px-4 py-2 text-base" role="menuitem" tabindex="-1" id="menu-item-4">Help</a>
                      </div>

                      <div className='flex items-center ml-5 hover:bg-stone-100'>
                         <svg className='w-5 h-5' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-info-square" viewBox="0 0 16 16"><path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/><path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/></svg>
                         <a href="#" className="text-gray-700 block px-4 py-2 text-base" role="menuitem" tabindex="-1" id="menu-item-5">Send feedback</a>
                      </div>
                        
                      </div>
                </div>
                </div>}
            </div>
            
        </div>
    </div>
  )
}

export default Navbar;