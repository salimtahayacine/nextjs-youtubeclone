import React, { useState } from 'react';
//import ItemYoutube from './ItemYoutube';
import { VideoDatas,ShortDatas } from './data';

function Siderbar(props) {
  //const sidebar = document.querySelector('#sidebar');
  const [showsub,setShowsub]= useState(false);
  const [showplaylis,setShowplaylist]= useState(false);

  const showply = () => {
    setShowplaylist(!showplaylis);
  }

  const ShowmoreSubs = () => {
      setShowsub(!showsub)
  }
  return (
    <div className=''>
      <div className='lg:w-56 md:w-14 lg:h-[900px] sm:invisible '>
      
      </div>
      <div className=' md:mr-2 flex-col top-12 fixed h-[900px]  lg:w-56 SideBare grid solid  pt-6 font-sans lg:text-sm md:text-xs  lg:overflow-y-scroll lg:overflow-x-hidden md:bg-white '>
          <div id='sidebar' className='lg:w-56  md:w-16 sm:w-0 md:visible sm:invisible justify-start lg:space-y-2 '>
          <div className='md:pt-7'>
            <div className=' lg:visible md:visible sm:invisible lg:items-center  lg:flex md:grid md:grid-rows-2 md:px-1 lg:px-4 my-2 h-9 items-center hover:bg-gray-200  lg:bg-gray-200 lg:mb-0 md:mb-12'>
              <svg className="lg:w-6 lg:h-6 md:w-6 md:h-6 lg:mx-2  md:mx-3  items-center " fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"></path></svg>
              <h4 className='lg:px-3 lg:text-sm md:text-xs md:mx-3 md:visible '>Home</h4>
            </div>
            <div className=' lg:visible md:visible sm:invisible lg:flex md:grid md:grid-rows-2 md:px-1 lg:px-4 my-2 h-9 items-center hover:bg-gray-200 lg:mb-0 md:mb-12'>
              <img className='lg:w-6 lg:h-6 md:w-6 md:h-6  lg:mx-2  md:mx-3 items-center' src='./compass.png'></img>
              <h4 className='lg:px-3 lg:text-sm md:text-xs md:mx-2  font-sans md:visible '>Explore</h4>
            </div>
            <div className='lg:visible md:visible sm:invisible lg:flex md:grid md:grid-rows-2 md:px-1 lg:px-4 my-2 h-9 items-center hover:bg-gray-200 lg:mb-0 md:mb-12'>
              <img className='lg:w-6 lg:h-6 md:w-6 md:h-6  lg:mx-2  md:mx-3 items-center' src='./shortYTB.png'></img>
              <h4 className='lg:px-3  lg:text-sm md:text-xs md:mx-2 font-sans md:visible'>Shorts</h4>
            </div>
            <div className='lg:visible md:visible sm:invisible lg:flex md:grid md:grid-rows-2 md:px-1 lg:px-4 my-2 h-9 items-center hover:bg-gray-200 lg:mb-0 md:mb-12'>
            <img className=' lg:w-6 lg:h-6 md:w-6 md:h-6  lg:mx-2  md:mx-3 items-center' src='./subscribe.png'></img>
              <h4 className='lg:px-3 lg:text-sm md:text-xs lg:mx-2 font-sans md:visible '>subscriptions</h4>
            </div>
            {/* <ItemYoutube className="flex " title='Home' imge={<svg className="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"></path></svg>}/>
            <ItemYoutube title='Explore' imge={<svg className="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 11H5m14 0a2 2 0 012 2v6a2 2 0 01-2 2H5a2 2 0 01-2-2v-6a2 2 0 012-2m14 0V9a2 2 0 00-2-2M5 11V9a2 2 0 012-2m0 0V5a2 2 0 012-2h6a2 2 0 012 2v2M7 7h10"></path></svg>}/>
            <ItemYoutube title='Trending' imge={<svg className="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 18.657A8 8 0 016.343 7.343S7 9 9 10c0-2 .5-5 2.986-7C14 5 16.09 5.777 17.656 7.343A7.975 7.975 0 0120 13a7.975 7.975 0 01-2.343 5.657z"></path><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9.879 16.121A3 3 0 1012.015 11L11 14H9c0 .768.293 1.536.879 2.121z"></path></svg>}/>
            <ItemYoutube title='Subscriptions' imge={<svg className="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z"></path></svg>}/> */}
        </div>
        
        <hr className='lg:py-1 lg:w-56 md:w-0  '/>
  
        <div className='autreslide lg:w-56 md:w-0  '>
            <div className='lg:flex md:grid md:grid-rows-2 md:px-1 lg:px-4 my-2 h-9 items-center hover:bg-gray-200 lg:mb-0'>
                 <img className='lg:w-6 lg:h-6 md:w-6 md:h-6 lg:mx-2 md:mx-3 items-center' src='./library.png'></img>
                 <h3 className='lg:px-2 lg:text-sm md:text-xs md:mx-2 '>Library</h3>
            </div>
            <div className='flex px-4 my-2 h-9 items-center hover:bg-gray-200 lg:visible md:invisible sm:invisible'>
                 <img className='  w-6 h-6 mx-2' src='./history.png'></img>
                 <h3 className='lg:px-3 md:px-8  rounded-lg'>History</h3>
            </div>
            <div className=' flex px-4 my-2 h-9 items-center hover:bg-gray-200 lg:visible md:invisible sm:invisible'>
                  <img className=' w-6 h-6 mx-2' src='./yourvideo.png'></img>
                  <h3 className='lg:px-3 md:px-8  rounded-lg'>Your videos</h3>
            </div>
            <div className='flex px-4 my-2 h-9 items-center hover:bg-gray-200 lg:visible md:invisible sm:invisible'>
                 <svg className=" w-6 h-6 mx-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                 <h3 className='lg:px-3 md:px-8 rounded-lg'>Watch later</h3>
            </div>
            <div className=' flex px-4 my-2 h-9 items-center hover:bg-gray-200 lg:visible md:invisible sm:invisible'>
                 <img className=' w-6 h-6 mx-2' src='./playlist.png'></img>
                 <h3 className=' lg:px-3 md:px-8  rounded-lg'>React JS</h3>
            </div>
            { showplaylis &&  <div>
                 <div className=' flex px-4 my-2 h-9 items-center hover:bg-gray-200 lg:visible md:invisible sm:invisible'>
                    <img className=' w-6 h-6 mx-2' src='./playlist.png'></img>
                    <h3 className=' lg:px-3 md:px-8  rounded-lg'>Css</h3>
                 </div>
                 <div className=' flex px-4 my-2 h-9 items-center hover:bg-gray-200 lg:visible md:invisible sm:invisible'>
                    <img className=' w-6 h-6 mx-2' src='./playlist.png'></img>
                    <h3 className=' lg:px-3 md:px-8  rounded-lg'>JavaScript</h3>
                 </div>
                 <div className=' flex px-4 my-2 h-8 items-center hover:bg-gray-200 lg:visible md:invisible sm:invisible'>
                    <img className=' w-6 h-6 mx-2' src='./playlist.png'></img>
                    <h3 className=' lg:px-3 md:px-8  rounded-lg'>Games</h3>
                 </div>
                 <div className=' flex px-4 my-2 h-8 items-center hover:bg-gray-200 lg:visible md:invisible sm:invisible'>
                    <img className=' w-6 h-6 mx-2' src='./playlist.png'></img>
                    <h3 className=' lg:px-3 md:px-8  rounded-lg'>Music</h3>
                 </div>
                 <div className=' flex px-4 my-2 h-8 items-center hover:bg-gray-200 lg:visible md:invisible sm:invisible'>
                    <img className=' w-6 h-6 mx-2' src='./playlist.png'></img>
                    <h3 className=' lg:px-3 md:px-8  rounded-lg'>Rap Morocco</h3>
                 </div>
            </div>}
            <div onClick={showply} className=' flex px-4 my-2 h-8 items-center hover:bg-gray-200 lg:visible md:invisible sm:invisible'>
                <svg className=" w-6 h-6 mx-2 cursor-pointer" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path></svg>
                 <h3 className=' lg:px-3 md:px-8  rounded-lg cursor-pointer'>{showplaylis===true ? 'Show less' : 'Show More'}</h3>
                 {/* <h3 className=' lg:px-3 md:px-8  rounded-lg'>Show More</h3> */}
            </div>
        </div>
        <hr className='lg:py-1 lg:w-56 md:w-0  '/>
        <div className='subs lg:w-56 md:w-0 lg:px-3  lg:visible md:invisible sm:invisible'>
             <div className='px-4 lg:w-56 md:w-0  '>
               <h3 className='lg:px-1 text-md lg:uppercase  text-gray-600  '>Subscriptions</h3>
             </div>
             
             <div className='pt-2  py-1'>
               
             <div>
              {VideoDatas.map((VideoData) => 
                 <div className=' grid grid-cols-3 px-2 my-2 h-8 items-center justify-center hover:bg-gray-200'>
                   <img className=' rounded-xl col-span-1 w-7 h-7 mx-1 ' src={VideoData.chaineAvatar} ></img>
                   <h4 className=' text-sm  hover:bg-gray-200  ' >{VideoData.chaineName}</h4>
                   <img className=' ml-12 w-5 h-5' src='./live.png'></img>
                 </div>
              )}
              </div>
              {showsub &&
                <div>
                    <div className=' flex px-1 my-2 h-8 items-center hover:bg-gray-200'>
                      <img className='w-7 h-7 mx-2' src='./avatar2.svg'></img>
                      <h4 className=' text-sm mx-4 '>Chaine 1</h4> 
                      <img className=' ml-12 w-5 h-5' src='./live.png'></img>
                    </div>
                    <div className=' flex px-1 my-2 h-8 items-center hover:bg-gray-200'>
                      <img className='w-7 h-7 mx-2' src='./avatar2.svg'></img>
                      <h4 className=' text-sm mx-4 '>Chaine 1</h4> 
                      <img className=' ml-12 w-5 h-5' src='./live.png'></img>
                    </div>
                    <div className=' flex px-1 my-2 h-8 items-center hover:bg-gray-200'>
                      <img className='w-7 h-7 mx-2' src='./avatar2.svg'></img>
                      <h4 className=' text-sm mx-4 '>Chaine 1</h4> 
                      <img className=' ml-12 w-5 h-5' src='./live.png'></img>
                    </div>
                    <div className=' flex px-1 my-2 h-8 items-center hover:bg-gray-200'>
                      <img className='w-7 h-7 mx-2' src='./avatar2.svg'></img>
                      <h4 className=' text-sm mx-4 '>Chaine 1</h4> 
                      <img className=' ml-12 w-5 h-5' src='./live.png'></img>
                    </div>
                    <div className=' flex px-1 my-2 h-8 items-center hover:bg-gray-200'>
                      <img className='w-7 h-7 mx-2' src='./avatar2.svg'></img>
                      <h4 className=' text-sm mx-4 '>Chaine 1</h4> 
                      <img className=' ml-12 w-5 h-5' src='./live.png'></img>
                    </div>
                </div>
               }
               <div onClick={ShowmoreSubs} className=' flex px-1 my-2 h-8 items-center hover:bg-gray-200 cursor-pointer'>
                  <svg className=" rounded-xl w-7 h-6 mx-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path></svg>
                  <h4 className=' text-sm mx-4 '>{showsub===true ? 'Show less' : 'Show More'}</h4> 
               </div>
               
               
             </div>
        </div>
        <hr className='lg:py-1 lg:w-56 md:w-0  '/>
        <div className='Explore py-1 lg:w-56 md:w-0 lg:px-2 md:px-24 lg:visible md:invisible sm:invisible'>
              <h4 className=' pb-2 px-7 text-md uppercase text-gray-600'>Explore</h4>
              <div className='  flex px-4 my-2 h-8 items-center hover:bg-gray-200'>
                  <img className='w-6 h-6 mx-2' src='./joystick.png'></img>
                  <h4 className='ml-4'>Gaming</h4>
              </div>
              <div className=' flex px-4 my-2 h-8 items-center hover:bg-gray-200'>
                  <img className='w-6 h-6 mx-2' src='./cup.png'></img>
                  <h4 className='ml-4'>Sports</h4>
              </div>
  
            </div>
        <hr className='lg:py-1 lg:w-56 md:w-0  '/>
        <div className='More-from-youtube lg:w-56 md:w-0 lg:px-3 md:px-24 lg:visible md:invisible sm:invisible'>
          <h4 className='pb-2 px-5 text-md uppercase text-gray-600'>More From Youtube</h4>
          <div className='flex px-3 my-2 h-8 items-center hover:bg-gray-200'>
            <img className='w-6 h-6 mx-2' src='./youtube-studio.png'></img>
            <h4 className='ml-4'>Creator Studio</h4>
          </div>
          <div className='flex px-3 my-2 h-8 items-center hover:bg-gray-200'>
          <img className='w-6 h-6 mx-2' src='./youtube-music.png'></img>
            <h4 className='ml-4'>YouTube Music</h4>
          </div>
          <div className='flex px-3 my-2 h-8 items-center hover:bg-gray-200'>
          <img className='w-6 h-6 mx-2' src='./youtube-kids.png'></img>
            <h4 className='ml-4'>YouTube Kids</h4>
          </div>
          <div className='flex px-3 my-2 h-8 items-center hover:bg-gray-200'>
          <img className='w-6 h-6 mx-2' src='./youtube-tv.png'></img>
            <h4 className='ml-4'>YouTube TV</h4>
          </div>
        </div>    
        <hr className=' lg:py-1 lg:w-56 md:w-0  '/>
        <div className=' py-1 lg:px-3 md:px-24 lg:visible md:invisible sm:invisible'>
            <div className='flex mx-2 px-2 my-2 h-8 items-center hover:bg-gray-200'>
              <svg className="w-5 h-6 mx-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"></path><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path></svg>
              <h3 className='ml-4'>Settings</h3>
            </div>
            <div className=' mx-2 px-2 my-2 h-8 flex items-center cursor-pointer hover:bg-gray-300  w-full'>
              <svg className="w-5 h-6 mx-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 21v-4m0 0V5a2 2 0 012-2h6.5l1 1H21l-3 6 3 6h-8.5l-1-1H5a2 2 0 00-2 2zm9-13.5V9"></path></svg>
              <h3 className='ml-4'>Report history</h3>
            </div >
            <div className=' mx-2 px-2 my-2 h-8 flex items-center cursor-pointer hover:bg-gray-300  w-full'>
              <svg className="w-5 h-6 mx-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8.228 9c.549-1.165 2.03-2 3.772-2 2.21 0 4 1.343 4 3 0 1.4-1.278 2.575-3.006 2.907-.542.104-.994.54-.994 1.093m0 3h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
              <h3 className='ml-4'>Help</h3>
            </div>
            <div className=' mx-2 px-2 my-2 h-8 flex items-center cursor-pointer hover:bg-gray-300  w-full'>
              <svg className="w-5 h-6 mx-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 15v-1a4 4 0 00-4-4H8m0 0l3 3m-3-3l3-3m9 14V5a2 2 0 00-2-2H6a2 2 0 00-2 2v16l4-2 4 2 4-2 4 2z"></path></svg>
              <h3 className='ml-4'>Send feedback</h3>
            </div>
        </div>
        <hr className=' lg:py-1 lg:w-56 md:w-0  '/>
        <div className='py-1 lg:px-3 md:px-24 lg:visible md:invisible sm:invisible'>
          <h4 className='mx-4 text-gray-600 text-xs cursor-pointer'>About <span>Press</span> <span>Copyright</span></h4>
          <h4 className='mx-4 text-gray-600 text-xs cursor-pointer'>Contact us <span> Creator</span></h4>
          <h4 className='mx-4 text-gray-600 text-xs cursor-pointer'>Advertise <span>Developers</span></h4>
        </div>
        <div className='py-1 lg:px-3 md:px-24 lg:visible md:invisible sm:invisible'>
          <h4 className='mx-4 text-gray-600 text-xs cursor-pointer'>Terms <span>Privacy</span> <span>Policy & Safety</span></h4>
          <h4 className='mx-4 text-gray-600 text-xs cursor-pointer'>How YouTube works</h4>
          <h4 className='mx-4 text-gray-600 text-xs cursor-pointer'>Test mew features</h4>
        </div>
  
        <div className=' py-1 mx-4 items-center lg:px-3 md:px-24 lg:visible md:invisible sm:invisible'>
          <h5 className=' text-gray-500 text-xs'>© 2022 Google LLC</h5>
        </div>
          </div>
      </div>
    </div>
  )
}

export default Siderbar