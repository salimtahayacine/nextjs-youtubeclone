import React from 'react'


function SecondBar() {

  return (
    // <div className='horizontalScroll w-[1400px]  lg:ml-4 md:ml-4 pt-14  bg-white  overflow-x-scroll font-sans'></div>
    <div className='horizontalScroll lg:w-[1400px] md:w-[970px] md1:w-[1200px] sm:w-[450px] lg:ml-2 md:ml-2 pt-14  bg-white   font-sans'>
        <hr />
        {/* <nav className='flex items-center md:mx-6  text-md lg:max-w-4xl ml-12' ></nav> */}
        <nav className='horizontalScrollNav   flex items-center text-md ' >
            <button  className='border-none rounded-full hover:scale-110' id='btn-scroll-left'><svg className="w-6 h-6 mx-2 cursor-pointer hover:scale-105" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd"></path></svg></button>
            <ul className='Suggest-countainer overflow-x-auto  flex lg:mx-1 p-1 justify-start items-center '>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-black text-white'>All</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7    bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>Music</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-5 py-1 my-2 h-7    bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>ReactJS</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>Tailwind </li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>CSS</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>house</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>Redux</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>BootStrap</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>Kotlin</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>HTML</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-6 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>Android.Studio</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>Laravel</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>MaterielUI</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>NodeJS</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>Php</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>Java</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>Python</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-5 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>MaterielUI</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>NodeJS</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>Php</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>Java</li>
                <li className=' cursor-pointer mx-1 border rounded-2xl px-3 py-1 my-2 h-7   bg-stone-100 hover:bg-stone-200  checked:bg-black checked:text-white'>Python</li>
                {/* We can use cnst states and call them by using map */}   
            </ul>
               <button  className='z-1' id='btn-scroll-right'><svg className="w-6 h-6 mx-2 cursor-pointer hover:scale-105" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path></svg></button>  
        </nav>
        <hr />
    </div>
  )
}

export default SecondBar